/*--------------------------------------------------------------------
  This file is part of the PE1MEW TTNMapper node.
  
  The PE1MEW TTNMapper node is free software: 
  you can redistribute it and/or modify it under the terms of a Creative 
  Commons Attribution-NonCommercial 4.0 International License 
  (http://creativecommons.org/licenses/by-nc/4.0/) by 
  PE1MEW (http://pe1mew.nl) E-mail: pe1mew@pe1mew.nl

  The PE1MEW TTNMapper node is distributed in the hope that 
  it will be useful, but WITHOUT ANY WARRANTY; without even the 
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
  PURPOSE.
  --------------------------------------------------------------------*/

/*!
   
 \file HAN_IoT_MouseTrap.h
 \brief Example implementation of a LoRaWAN mouse trap  
 \author Remko Welling (WLGRW) remko.welling@han.nl 
 \version See revision table
 
 Version | Date       | comment
 --------|------------|----------------------------
 0.1     | 27-12-2019 | Initial version
         |            | 
         |            | 
         |            | 
 
 */
#ifndef __HAN_IOT_MOUSE_TRAP_H_
#define __HAN_IOT_MOUSE_TRAP_H_

#include <stdint.h>
#include <Arduino.h>  // millis()

/// \brief Timer class
class HAN_IoT_MouseTrap
{
//variables
public:
protected:
private:

  /// end time at which timer shall expire
  unsigned long _msEndTime;

  /// State of the timer, true = running, false = expired.
  bool _active;
   
//functions
public:
  HAN_IoT_MouseTrap();
  ~HAN_IoT_MouseTrap();
  
protected:
private:
}; // HAN_IoT_MouseTrap

#endif //__HAN_IOT_MOUSE_TRAP_H_
